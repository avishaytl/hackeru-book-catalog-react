import React from 'react';
import Main from './pages/Main';
import Login from './pages/Login';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Helmet } from 'react-helmet';
const TITLE = 'Book Catalog - Avishay Tal';
export default class App extends React.Component{
  render(){
    return (
      <Router> 
        <Helmet>
          <title>{ TITLE }</title>
        </Helmet>
        <Route exact path="/">
          <Login />
        </Route>
        <Route path="/BookCatalog">
          <Main />
        </Route> 
      </Router>
    );
  }
}


