import { takeLatest } from "redux-saga/effects";
import  * as sagas from "./sagas.js";
import primaryValues from '../constants/Values.js';
export function* rootSaga() {
  yield takeLatest(primaryValues.$GET_SIGNIN, sagas.fetchSignin);
  yield takeLatest(primaryValues.$GET_SIGNUP, sagas.fetchSignup);
  yield takeLatest(primaryValues.$GET_BOOKS, sagas.fetchBooks);
}