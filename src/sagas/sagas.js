import { put ,call } from "redux-saga/effects";
import primaryValues from '../constants/Values.js'; 

const postRequest = (key, body, url = 'http://127.0.0.1:8000/') =>
    Promise.race([
        fetch(url + key, {
            method: 'POST',
            headers: {
            //   'Access-Control-Allow-Origin': 'http://localhost:3000/',
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
          }).then(response => response.json())
          .then(responseJson => responseJson)
          .catch(error =>  console.debug('Request_error ',error)),new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), 20000))]);
const getRequest = (key, url = 'http://127.0.0.1:8000/') =>
    Promise.race([
        fetch(url + key).then(response => response.json())
        .then(responseJson => responseJson)
        .catch(error =>  console.debug('Request_error ',error)),new Promise((_, reject) => setTimeout(() => reject(new Error('timeout')), 20000))]);

export function* fetchSignin( action ) {
    let key = 'fetchSignin'; 
    let body = action.params;
    console.log(key);
    try{
        const response = yield call(postRequest, key, body);
        if(response.success){
            console.log('response' , response); 
            yield put({type: primaryValues.$SIGNIN_RESULT, response});
        }else{
            console.log('response' , response);
            yield put({type: primaryValues.$SIGNUP_ERROR, response});
        }
    }catch(e){
        console.log('fetchSignin error' + e.toString());
    }
}
export function* fetchSignup( action ) {
    let key = 'fetchSignup';
    let body = action.params;
    console.log(key,body);
    try{
        const response = yield call(postRequest, key, body);
        if(response.success){
            console.log('response' , response); 
            yield put({type: primaryValues.$SIGNUP_RESULT, response});
        }else{
            console.log('response' , response);
            yield put({type: primaryValues.$SIGNUP_ERROR, response});
        }
    }catch(e){
        console.log('fetchSignup error' + e.toString());
    }
}
export function* fetchBooks( action ) {
    let key = 'fetchBooks'; 
    try{
        const response = yield call(getRequest, key);
        if(response.success){
            console.log('response' , response); 
            let booksRes = setActiveBooks(response[0]);
            yield put({type: primaryValues.$BOOKS_RESULT, booksRes});
        }else{
            console.log('response' , response);
            yield put({type: primaryValues.$BOOKS_ERROR, response});
        }
    }catch(e){
        console.log('fetchBooks error' + e.toString());
    }
}
function setActiveBooks(books){
    let bookResult = [];
    for(let i = 0;i < books.length;i++){
        bookResult.push({key:'book' + (i + 1),url_img:books[i].url_img,book_name:books[i].name,description: books[i].description},)
    }
    console.log(bookResult);
    return bookResult;
}