import React from 'react'; 
import '../App.css'; 
import { connect } from "react-redux";
import { Redirect } from 'react-router'; 
import Pagination from '@material-ui/lab/Pagination';
import anime from 'animejs/lib/anime.es.js';
import * as Icons from 'react-icons/all'; 
import Modal from '../components/ModalApp';
import primaryValues from '../constants/Values.js';
class Main extends React.Component{
  constructor(props){
    super(props);
    this.state={ 
      countPage: 0,
      activeIndex: 0, 
      modalActive: false,
      leftColView:{ 
        url_img: 'img',
        book_name: 'book_name',
        description: 'description'
      },
      midColView:{ 
        url_img: 'img',
        book_name: 'book_name',
        description: 'description' 
      },
      rightColView:{ 
        url_img: 'img',
        book_name: 'book_name',
        description: 'description' 
      }, 
      main_books_view: this.props.initialState.books !== [] ? this.props.initialState.books : [
        {key:'book1',url_img:'url_img',book_name:'book_name1',description:'description'},
        {key:'book2',url_img:'url_img',book_name:'book_name2',description:'description'},
        {key:'book3',url_img:'url_img',book_name:'book_name3',description:'description'}, ],
      main_books:[ 
      ]
    }
    let countPages = parseInt(this.state.main_books_view.length / 3);
    if(parseInt(this.state.main_books_view.length % 3) !== 0)
      countPages++;
    this.state.countPage = countPages; 
    for (const book in this.state.main_books_view) {  
      this.state.main_books.push(<div key={this.state.main_books_view[book].key} style={{flexGrow:1}}>{this.state.main_books_view[book].key}</div>);
    }
    if(this.state.main_books_view[0] !== undefined){
      this.state.leftColView.url_img = this.state.main_books_view[0].url_img;
      this.state.leftColView.book_name = this.state.main_books_view[0].book_name;
      this.state.leftColView.description = this.state.main_books_view[0].description;
      this.state.midColView.url_img = this.state.main_books_view[1].url_img;
      this.state.midColView.book_name = this.state.main_books_view[1].book_name;
      this.state.midColView.description = this.state.main_books_view[1].description;
      this.state.rightColView.url_img = this.state.main_books_view[2].url_img;
      this.state.rightColView.book_name = this.state.main_books_view[2].book_name;
      this.state.rightColView.description = this.state.main_books_view[2].description; 
    }
    this.setActiveColView = this.setActiveColView.bind(this); 
    this.openModal = this.openModal.bind(this);
  }     
  onChangePagination = (page) =>{
    console.log(page);
    anime({
    targets: '.pagination-view', 
    marginTop: 130,
    opacity: 0,
    duration: 1000, 
    });
    setTimeout(() => {
      anime({
      targets: '.pagination-view', 
      marginTop: 0,
      opacity: 1,
      duration: 1000, 
      }); 
      this.setActiveColView(page);
    }, 200);
  }
  setActiveColView = (page) =>{ 
    let index;
    if(page === 1)
      index = 0;
    else
      index = (page - 1) * 3; 
    if(this.state.main_books_view[index] !== undefined)
      this.setState({leftColView:{ 
        url_img: this.state.main_books_view[index].url_img,
        book_name: this.state.main_books_view[index].book_name,
        description: this.state.main_books_view[index].description}});
    else
      this.setState({leftColView:{ 
        url_img: '',
        book_name: '',
        description: ''}});

    if(this.state.main_books_view[index + 1] !== undefined)
      this.setState({midColView:{ 
        url_img: this.state.main_books_view[index + 1].url_img,
        book_name: this.state.main_books_view[index + 1].book_name,
        description: this.state.main_books_view[index + 1].description}});
    else
      this.setState({midColView:{ 
        url_img: '',
        book_name: '',
        description: ''}});
    if(this.state.main_books_view[index + 2] !== undefined)
      this.setState({rightColView:{ 
        url_img: this.state.main_books_view[index + 2].url_img,
        book_name: this.state.main_books_view[index + 2].book_name,
        description: this.state.main_books_view[index + 2].description}}); 
    else
      this.setState({rightColView:{ 
        url_img: '',
        book_name: '',
        description: ''}});
  }
  openModal = (val) =>{  
    this.setState({modalActive: val});  
  }
  render(){ 
    return (
      <div className='App' style={{backgroundColor:'#0e0e0e'}}>
            {this.state.modalActive ? <Modal func={this.openModal} book_name_name={this.props.initialState.user_name.first_name}/> : null}
            <div className='main-view'> 
              <Pagination onChange={(event,page)=>this.onChangePagination(page)} count={this.state.countPage} />
              <div className='pagination-view'>
                <div className='pagination-view-page'>
                  <div className='pagination-view-page-inside' style={{fontSize:12}}>{this.state.leftColView.url_img}
                  </div>
                  <div className='pagination-view-page-inside'>{this.state.leftColView.book_name}{this.state.leftColView.description}</div>
                </div>
                <div className='pagination-view-page'> 
                  <div className='pagination-view-page-inside' style={{fontSize:12}}>{this.state.midColView.url_img}</div>
                  <div className='pagination-view-page-inside'>{this.state.midColView.book_name} {this.state.midColView.description}</div>
                </div>
                <div className='pagination-view-page'>
                  <div className='pagination-view-page-inside' style={{fontSize:12}}>{this.state.rightColView.url_img}</div>
                  <div className='pagination-view-page-inside'>{this.state.rightColView.book_name} {this.state.rightColView.description}</div>
                </div>
              </div>
            </div>
          <div className='main-slider-view'> 
            <div className='add-book-view'>
              <div className='add-book-btn'>
                <Icons.MdAddCircleOutline onClick={()=>this.setState({modalActive: this})} style={{fontSize:40,color:'wheat',cursor:'pointer'}}/> 
                <p style={{margin:0,fontSize:12,color:'wheat',width:86}}>Add new book</p>
              </div>
            </div>
            {this.state.main_books}
          </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    initialState: state.initialState,
  };
};
const mapDispachToProps = dispatch => {
  return { 
    fetchBooks: (info) => dispatch(info)
  };
};
export default connect(mapStateToProps,mapDispachToProps)(Main);