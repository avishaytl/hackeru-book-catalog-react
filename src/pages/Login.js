import React from 'react'; 
import '../App.css'; 
import book1 from '../style/images/book1.webp';
import book2 from '../style/images/book2.webp';
import book3 from '../style/images/book3.webp';
import book4 from '../style/images/book4.webp';
import Input from '../components/InputApp';
import Button from '../components/ButtonApp';
import { connect } from "react-redux";
import { Redirect } from 'react-router'; 
import anime from 'animejs/lib/anime.es.js';
import * as Icons from 'react-icons/all';
import { Scrollbars } from 'react-custom-scrollbars';
import primaryValues from '../constants/Values.js';
class Login extends React.Component{
  constructor(props){
    super(props);
    this.state={
      email: '',
      password: '',
      retype_password: '',
      phone: '',
      first_name: '',
      last_name: '',
      error_msg: 'all fields is must',
      go_main: false,
      signupCheckbox: false,
      checkboxTextColor: 'rgba(0,0,0,0)',
    }
    this.changeState = this.changeState.bind(this);
    this.btnSignin = this.btnSignin.bind(this);
    this.btnSignup = this.btnSignup.bind(this); 
  } 
  onPressBtn = (name) =>{ 
    switch(name){
        case 'signin':
            anime({
            targets: '#login-signin', 
            left: 0,
            duration: 1000, 
            });
            anime({
            targets: '#login-btn-view', 
            top: 350,
            duration: 1000, 
            });
            break;
        case 'signup':
            anime({
            targets: '#login-signup', 
            right: 0,
            duration: 1000, 
            });
            anime({
            targets: '#login-btn-view', 
            top: 350,
            duration: 1000, 
            });
            break;
        case 'back-signin':
            anime({
            targets: '#login-signin', 
            left: 1000,
            duration: 1000, 
            });
            anime({
            targets: '#login-btn-view', 
            top: 110,
            duration: 1000, 
            });
            break;
        case 'back-signup':
            anime({
            targets: '#login-signup', 
            right: 1000,
            duration: 1000, 
            });
            anime({
            targets: '#login-btn-view', 
            top: 110,
            duration: 1000, 
            });
            break;
        default:
          break;
    } 
  } 
  changeState = (val,label) => { 
    switch(label){
      case 'Email':
        this.setState({email: val.target.value});
        break;
      case 'Password':
        this.setState({password: val.target.value});
        break;
      case 'Phone':
        this.setState({phone: val.target.value});
        break;
      case 'Retype Password':
        this.setState({retype_password: val.target.value});
        break;
      case 'First Name':
        this.setState({first_name: val.target.value});
        break;
      case 'Last Name':
        this.setState({last_name: val.target.value});
        break; 
      default:
        break;
    }
  } 
  btnSignin = () =>{ 
    let params = {
      email: this.state.email,
      password: this.state.password, 
    };
    if(params.email === '' || params.password === ''){
      this.setState({checkboxTextColor: 'red'}); 
    }else{ 
      this.props.fetchSignin({type: primaryValues.$GET_SIGNIN, params});
      this.props.fetchBooks({type: primaryValues.$GET_BOOKS}); 
      this.setState({go_main: true}); 
    }
  }
  btnSignup = () =>{
    let formValidate = true;  
    let params = {
      email: this.state.email,
      password: this.state.password,
      phone: this.state.phone,
      first_name: this.state.first_name,
      last_name: this.state.last_name,
      status: this.state.signupCheckbox ? 0 : 1,
    };
    for (const field in this.state) { 
      if(field !== 'signupCheckbox' && field !== 'go_main' && field !== 'checkboxTextColor'&& field !== 'error_msg')
        if(this.state[field] === ''){
          this.setState({checkboxTextColor: 'red'});
          formValidate = false;
        }
    }
    if(this.state.password !== this.state.retype_password){
      this.setState({error_msg: 'password must be same'});
      this.setState({checkboxTextColor: 'red'});
    }
    else
      if(formValidate){ 
        this.props.fetchSignup({type: primaryValues.$GET_SIGNUP, params});
        this.props.fetchBooks({type: primaryValues.$GET_BOOKS}); 
        this.setState({go_main: true}); 
      }
  }
  render(){
    if (this.state.go_main && this.props.initialState.login_success && this.props.initialState.books_success) { 
        return <Redirect to={{pathname: '/BookCatalog'}}/>;  
    }
    return (
      <div className='App'>
          <div className='login-background'>
            <img src={book1} className='login-img' alt="book1_error"/>
            <img src={book2} className='login-img' alt="book2_error"/>
            <img src={book3} className='login-img' alt="book3_error"/>
            <img src={book4} className='login-img' alt="book4_error"/>
          </div>
          <div className='login-view'>
              <div className='login-title-view'>
                <h1 style={{margin:0,fontSize:'2.5vw'}} className='login-title'>Book Catalog</h1>
                <p style={{margin:0,fontSize:'1.1vw',paddingBottom:2}} className='login-title'>By Avishay Tal</p> 
              </div>
              <div id='login-signin'>
                <Icons.MdKeyboardArrowRight onClick={()=>this.onPressBtn('back-signin')} style={{fontSize:100,color:'black',position:'absolute',left:10,cursor:'pointer'}}/>
                <Input onChange={this.changeState} icon={<Icons.FaUserAlt style={{color:'black',fontSize:20,margin:5}}/>} type={'text'} label={'Email'}/>
                <Input onChange={this.changeState} icon={<Icons.FaLock style={{color:'black',fontSize:20,margin:5}}/>} type={'password'} label={'Password'}/>
                <Button label={'Signin'} onClick={this.btnSignin}/>
                <p style={{fontSize:12,color: this.state.checkboxTextColor,margin:0,marginBottom:10}}>{this.state.error_msg}</p>
              </div>
              <div id='login-signup'>
                <Icons.MdKeyboardArrowLeft onClick={()=>this.onPressBtn('back-signup')} style={{fontSize:100,color:'black',position:'absolute',right:10,cursor:'pointer'}}/>
                <div className='signup-form'>
                  <div className='signup-form-inside'>
                  <Scrollbars style={{ width: '100%', height: '100%'}}>
                    <Input onChange={this.changeState} icon={<Icons.FaUserAlt style={{color:'black',fontSize:20,margin:5}}/>} type={'text'} label={'First Name'}/>
                    <Input onChange={this.changeState} icon={<Icons.FaUserAlt style={{color:'black',fontSize:20,margin:5}}/>} type={'text'} label={'Last Name'}/>
                    <Input onChange={this.changeState} icon={<Icons.IoIosMail style={{color:'black',fontSize:25,margin:5}}/>} type={'text'} label={'Email'}/> 
                    <Input onChange={this.changeState} icon={<Icons.MdPhoneIphone style={{color:'black',fontSize:25,margin:5}}/>} type={'text'} label={'Phone'}/> 
                    <Input onChange={this.changeState} icon={<Icons.FaLock style={{color:'black',fontSize:20,margin:5}}/>} type={'password'} label={'Password'}/> 
                    <Input onChange={this.changeState} icon={<Icons.FaLock style={{color:'black',fontSize:20,margin:5}}/>} type={'password'} label={'Retype Password'}/>
                    <div style={{width:'100%',textAlign:'center'}}>
                      <div style={{width:'100%'}}>
                        <div onClick={()=>this.state.signupCheckbox === false ? this.setState({signupCheckbox: true}) : this.setState({signupCheckbox: false})} style={{marginTop:10}}>
                          <input type="checkbox" style={{cursor:'pointer'}} onChange={()=>this.state.signupCheckbox === false ? this.setState({signupCheckbox: true}) : this.setState({signupCheckbox: false})} checked={this.state.signupCheckbox}/>
                          <label style={{fontSize:12,cursor:'pointer'}}>are you ACTIVE author?</label> 
                        </div>
                      </div>
                      <Button label={'Signup'} onClick={this.btnSignup}/> 
                      <p style={{fontSize:12,color: this.state.checkboxTextColor,margin:0,marginBottom:10}}>{this.state.error_msg}</p>
                    </div>
                  </Scrollbars>
                  </div>
                </div>
           
              </div>
              <div id='login-btn-view'>
                <p onClick={()=>this.onPressBtn('signin')} className='login-btn'>Signin</p>
                <p onClick={()=>this.onPressBtn('signup')}  className='login-btn'>Signup</p>
              </div>
          </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    initialState: state.initialState,
  };
};
const mapDispachToProps = dispatch => {
  return {
    fetchSignup: (info) => dispatch(info),
    fetchSignin: (info) => dispatch(info),
    fetchBooks: (info) => dispatch(info)
  };
};
export default connect(mapStateToProps,mapDispachToProps)(Login);