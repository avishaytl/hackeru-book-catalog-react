import primaryValues from '../constants/Values.js';
const initialState = {
  user_name: null, 
  user_id: null,  
  loading: false,
  loading_success: false,
  login_success: false,
  books_success: false,
  books: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case primaryValues.$GET_SIGNIN:
      return{
        ...state, 
        loading: true,
      }
    case primaryValues.$SIGNIN_RESULT:
      return{
        ...state,
        user_name: action.response.name,
        user_id: action.response.id,
        login_success: true,
        loading_success: true,
        loading: false,
      }
    case primaryValues.$SIGNIN_ERROR:
      return{
        ...state, 
        loading_success: false,
        loading: false,
      } 
    case primaryValues.$GET_SIGNUP:
      return{
        ...state, 
        loading: true,
      }
    case primaryValues.$SIGNUP_RESULT:
      return{
        ...state, 
        loading_success: true,
        login_success: true,
        loading: false,
      }
    case primaryValues.$SIGNUP_ERROR:
      return{
        ...state, 
        loading_success: false,
        loading: false,
      } 
    case primaryValues.$GET_BOOKS:
      return{
        ...state,  
      }
    case primaryValues.$BOOKS_RESULT: 
      return{
        ...state, 
        books_success: true,
        books: action.booksRes,
      }
    case primaryValues.$BOOKS_ERROR:
      return{
        ...state,  
        books_success: false,
      } 
    default:
      return state;
  }
};

export default reducer;