export const primaryValues = {
    $GET_SIGNIN: 'GET_SIGNIN',
    $SIGNIN_RESULT: 'SIGNIN_RESULT',
    $SIGNIN_ERROR: 'SIGNIN_ERROR',
    $GET_SIGNUP: 'GET_SIGNUP',
    $SIGNUP_RESULT: 'SIGNUP_RESULT',
    $SIGNUP_ERROR: 'SIGNUP_ERROR', 
    $GET_BOOKS: 'GET_BOOKS',
    $BOOKS_RESULT: 'BOOKS_RESULT',
    $BOOKS_ERROR: 'BOOKS_ERROR', 
}
export default primaryValues;









