import React from 'react'; 
import Input from './InputApp';
// import Button from './ButtonApp'; 
// import anime from 'animejs/lib/anime.es.js';
import * as Icons from 'react-icons/all'; 
export default class ModalApp extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            id: '',
            book_name: '',
            book_description: '',
            errorMsgColor: 'rgba(0,0,0,0)',
            error_msg: 'all fields is must',
            signupCheckbox: false,
        }
        this.changeState = this.changeState.bind(this);
    }
    changeState = (val,label) => { 
      switch(label){
        case 'Book Name':
          this.setState({book_name: val.target.value});
          break;
        case 'Book Descreption':
          this.setState({book_description: val.target.value});
          break; 
        default:
          break;
      }
    } 
    onSubmit = ()=>{ 
        if(this.state.book_name === '' || this.state.book_description === ''){
            this.setState({errorMsgColor: 'red'});
            return true;
        }
        return false;
    }
    render(){
        switch(this.props.name){
            case 'loading':
            return( 
                <div className='modal-view'>
                    <label style={{position:'absolute',top:70,fontSize:14}}>{this.props.title_name}</label> 
                </div>
            ) 
            default:
                return(
                    <div className='modal-view'>
                        <div  onClick={()=>this.props.func(false)} className='modal-view'>
                        </div> 
                        <div className='modal-view-inside'> 
                                <div className='modal-view-form'>
                                    <p style={{margin:0,marginTop:10,fontSize:14}}>Hello dear {this.props.title_name}</p> 
                                    <Input onChange={this.changeState} icon={<Icons.FaBook style={{color:'black',fontSize:20,margin:5}}/>} type={'text'} label={'Book Name'}/>
                                    <Input onChange={this.changeState} icon={<Icons.FaBook style={{color:'black',fontSize:20,margin:5}}/>} type={'text'} label={'Book Descreption'}/> 
                                    <div style={{width:'100%',textAlign:'center'}}>
                                        <div style={{width:'100%'}}>
                                            <div onClick={()=>this.state.signupCheckbox === false ? this.setState({signupCheckbox: true}) : this.setState({signupCheckbox: false})} style={{marginTop:10}}>
                                                <input type="checkbox" style={{cursor:'pointer'}} onChange={()=>this.state.signupCheckbox === false ? this.setState({signupCheckbox: true}) : this.setState({signupCheckbox: false})} checked={this.state.signupCheckbox}/>
                                                <label style={{fontSize:12,cursor:'pointer'}}>is a ACTIVE book?</label> 
                                            </div>
                                        </div> 
                                    </div> 
                                    <div className='upload-image-view'> 
                                        <form onSubmit={(e) => {if(this.onSubmit()) e.preventDefault();}} action="http://127.0.0.1:8000/fetchNewBook" style={{alignItems:'center',textAlign:'center'}} method="post" encType="multipart/form-data"> 
                                            <input style={{fontSize:14,color:'black',fontWeight:'bold'}}  type="file" name="file" required/> 
                                            <input style={{fontSize:0,color:'rgba(0,0,0,0)',visibility:'hidden'}} onChange={()=>null} value={this.state.book_name} name="book_name"/> 
                                            <input style={{fontSize:0,color:'rgba(0,0,0,0)',visibility:'hidden'}} onChange={()=>null}  value={this.state.book_description} name="book_description"/> 
                                            <p style={{fontSize:12,color: this.state.errorMsgColor,margin:0,marginBottom:10}}>{this.state.error_msg}</p>
                                            <input className='button-app' type="submit" value="Add Book" name="submit"/>
                                        </form>
                                    </div>
                                </div> 
                        </div> 
                    </div>
                )
        }
    }
}