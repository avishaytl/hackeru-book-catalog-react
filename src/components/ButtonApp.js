import React from 'react'; 
export default class ButtonApp extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            id: '',
        }
    }
    render(){
        return(
            <button onClick={this.props.onClick} className='button-app'>{this.props.label}</button>
        );
    }
}